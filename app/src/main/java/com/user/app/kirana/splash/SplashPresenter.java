package com.user.app.kirana.splash;

import android.support.annotation.NonNull;

public interface SplashPresenter {

    void getLocationPermission();
    void checkLocationPermissionGranted(int requestCode, @NonNull int grantResults[]);
    void navigateAway();
}

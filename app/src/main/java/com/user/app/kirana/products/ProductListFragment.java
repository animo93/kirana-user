package com.user.app.kirana.products;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.user.app.kirana.R;
import com.user.app.kirana.adapter.ProductListAdapter;

public class ProductListFragment extends Fragment{

    public static final String CATEGORY_NAME = "category_name";
    private RecyclerView mRecyclerView;
    private ProductListAdapter productListAdapter;
    private ProductsPresenterImpl productsPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_product_list,container,false);
        productsPresenter = new ProductsPresenterImpl(getActivity());
        mRecyclerView = swipeRefreshLayout.findViewById(R.id.product_recycler_view);

        if (productsPresenter.getScreenWidthDp() >= 1200) {
            final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
            mRecyclerView.setLayoutManager(gridLayoutManager);
        } else if (productsPresenter.getScreenWidthDp() >= 800) {
            final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            mRecyclerView.setLayoutManager(gridLayoutManager);
        } else {
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(linearLayoutManager);
        }
        productListAdapter = new ProductListAdapter(getContext());
        mRecyclerView.setAdapter(productListAdapter);
        Bundle bundle = getArguments();

        productListAdapter.setProducts(productsPresenter.getProductByCategory(bundle.getString(CATEGORY_NAME)));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);

            }
        });
        return swipeRefreshLayout;
    }
}

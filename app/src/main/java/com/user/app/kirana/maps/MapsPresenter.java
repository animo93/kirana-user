package com.user.app.kirana.maps;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface MapsPresenter {

    void init();
    void updateLocationUI();
    void displayLocationSettingsRequest();
    void getDeviceLocation();
    void checkLocationSettings(int requestCode, int resultCode);
    void buildGoogleApiClient();
    LatLng getMyKirana(LatLng myLocation);
    List<LatLng> getAllNearbyKiranas(LatLng myLocation,int radius);
    void destroy();

}

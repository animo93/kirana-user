package com.user.app.kirana.maps;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.user.app.kirana.R;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final String LOG_TAG = MapsActivity.class.getSimpleName();

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private MapsPresenterImpl mapsPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapsPresenter = new MapsPresenterImpl(this);
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mapsPresenter.setmLastKnownLocation(
                    (Location) savedInstanceState.getParcelable(KEY_LOCATION));
            mapsPresenter.setmCameraPosition(
                    (CameraPosition) savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
            );
        }

        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps);

        mapsPresenter.init();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        if (mapsPresenter.getmMap() != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mapsPresenter.getmMap().getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mapsPresenter.getmLastKnownLocation());
            super.onSaveInstanceState(outState,outPersistentState);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mapsPresenter.setmMap(googleMap);

        // Turn on the My Location layer and the related control on the map.
        mapsPresenter.updateLocationUI();


    }


    /**
     * Handles the result after Location Settings have been turned on
     * @param requestCode
     * @param resultCode
     * @param data
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mapsPresenter.checkLocationSettings(requestCode,resultCode);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(LOG_TAG,"GoogleAPiClient connected");
        mapsPresenter.displayLocationSettingsRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        mapsPresenter.destroy();
        super.onDestroy();
    }

}

package com.user.app.kirana.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.user.app.kirana.R;
import com.user.app.kirana.products.Product;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>{
    private Context mContext;
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public ProductListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(parent instanceof RecyclerView){
            View view = LayoutInflater.from(mContext).inflate(
                    R.layout.card_product,
                    parent,
                    false
            );
            view.setFocusable(true);
            return new ProductListAdapter.ViewHolder(view);
        } else {
            throw new RuntimeException("Not bound to RecyclerView");
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Product product = getProducts().get(position);
        holder.nameTextView.setText(product.getName());
        holder.priceTextView.setText(String.valueOf(product.getPrice()));
        holder.quantityTextView.setText("1");
        holder.imageView.setImageResource(product.getImageId());
        holder.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String quantity = (String) holder.quantityTextView.getText();
                int intQuantity = Integer.parseInt(quantity);
                intQuantity++;
                holder.quantityTextView.setText(String.valueOf(intQuantity));
            }
        });
        holder.buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String quantity = (String) holder.quantityTextView.getText();
                int intQuantity = Integer.parseInt(quantity);
                if(intQuantity!=0){
                    intQuantity--;
                }

                holder.quantityTextView.setText(String.valueOf(intQuantity));
            }
        });

    }

    @Override
    public int getItemCount() {
        return products==null?0:products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView imageView;
        public final Button buttonAdd;
        public final Button buttonMinus;
        public final TextView quantityTextView;
        public final TextView nameTextView;
        public final TextView priceTextView;
        public final View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mView=itemView;
            this.imageView = itemView.findViewById(R.id.imageView);
            this.buttonAdd = itemView.findViewById(R.id.button_add);
            this.buttonMinus = itemView.findViewById(R.id.button_minus);
            this.quantityTextView = itemView.findViewById(R.id.quantity);
            this.nameTextView = itemView.findViewById(R.id.product_name);
            this.priceTextView = itemView.findViewById(R.id.product_price);
        }
    }
}

package com.user.app.kirana.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.user.app.kirana.R;

public class ProductsFragment extends Fragment {

    SearchView mSearchView;
    Fragment mFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_product,container,false);
        mSearchView = relativeLayout.findViewById(R.id.search);

        return relativeLayout;
    }
}

package com.user.app.kirana.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.user.app.kirana.R;

import java.util.List;

public class ProductCategoryAdapter extends BaseAdapter{

    private List<String> productCategories;
    private Context mContext;
    private static LayoutInflater inflater = null;

    public ProductCategoryAdapter(List<String> productCategories,Context mContext) {
        this.productCategories = productCategories;
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return productCategories==null?0:productCategories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View productView = inflater.inflate(R.layout.card_category,null);
        Holder holder = new Holder();
        holder.textView = productView.findViewById(R.id.product_category);
        holder.textView.setText(productCategories.get(position));
        /*TextView textView = new TextView(mContext);
        if(productCategories!=null){

            textView.setText(productCategories.get(position));
            textView.setGravity(Gravity.CENTER);
        }*/

        return productView;
    }

    public class Holder{
        TextView textView;
    }
}

package com.user.app.kirana.splash;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.user.app.kirana.R;

public class SplashActivity extends AppCompatActivity {

    private SplashPresenterImpl splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash);
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        splashPresenter = new SplashPresenterImpl(this);

        splashPresenter.getLocationPermission();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        splashPresenter.checkLocationPermissionGranted(requestCode,grantResults);
    }
}

package com.user.app.kirana.splash;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.user.app.kirana.maps.MapsActivity;

public class SplashPresenterImpl implements SplashPresenter {

    private static final String LOG_TAG = SplashPresenterImpl.class.getSimpleName();

    private SplashActivity mActivity;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    SplashPresenterImpl(SplashActivity activity){
        mActivity = activity;
    }
    @Override
    public void getLocationPermission() {
        Log.i(LOG_TAG,"inside getLocationPermission");
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(mActivity.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            setmLocationPermissionGranted(true);
            navigateAway();
            //displayLocationSettingsRequest();
        } else {
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void checkLocationPermissionGranted(int requestCode, @NonNull int[] grantResults) {
        Log.i(LOG_TAG,"inside onRequestPermissionsResult");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setmLocationPermissionGranted(true);
                    navigateAway();
                }
            }
        }
    }

    @Override
    public void navigateAway() {
        if(ismLocationPermissionGranted()){
            Intent intent = new Intent(mActivity, MapsActivity.class);
            mActivity.startActivity(intent);
            mActivity.finish();
        }
    }

    public boolean ismLocationPermissionGranted() {
        return mLocationPermissionGranted;
    }

    public void setmLocationPermissionGranted(boolean mLocationPermissionGranted) {
        this.mLocationPermissionGranted = mLocationPermissionGranted;
    }
}

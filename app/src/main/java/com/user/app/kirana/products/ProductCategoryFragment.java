package com.user.app.kirana.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.user.app.kirana.R;
import com.user.app.kirana.adapter.ProductCategoryAdapter;

public class ProductCategoryFragment extends Fragment{

    GridView mCategoriesGrid ;
    ProductsPresenterImpl productsPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        productsPresenter = new ProductsPresenterImpl(getActivity());
        ConstraintLayout constraintLayout = (ConstraintLayout) inflater.inflate(R.layout.fragment_product_categories,container,false);
        mCategoriesGrid = constraintLayout.findViewById(R.id.product_categories_grid);
        mCategoriesGrid.setAdapter(new ProductCategoryAdapter(productsPresenter.getProductCategories(),
                getContext()));
        mCategoriesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                productsPresenter.replaceCategoryFragment(position);
            }
        });
        return constraintLayout;
    }
}

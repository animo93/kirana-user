package com.user.app.kirana.products;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;

import com.user.app.kirana.R;

import java.util.ArrayList;
import java.util.List;

public class ProductsPresenterImpl implements ProductsPresenter{
    private FragmentActivity mActivity;
    private List<String> productCategories;
    private List<Product> products;

    public ProductsPresenterImpl(FragmentActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public List<String> getProductCategories() {
        productCategories = new ArrayList<>();
        productCategories.add("Fruits");
        productCategories.add("Vegetables");
        productCategories.add("Grocery");
        productCategories.add("Misc");
        return productCategories;
    }

    @Override
    public void replaceCategoryFragment(int postion) {
        ProductListFragment productListFragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(ProductListFragment.CATEGORY_NAME,productCategories.get(postion));
        productListFragment.setArguments(args);
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.product_fragment,productListFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public List<Product> getProductByCategory(String category) {
        products = getAllProducts();
        List<Product> toBeRemoved = new ArrayList<>();
        for(Product product:products){
            if(!product.getCategory().equals(category)){
                toBeRemoved.add(product);
            }
        }
        products.removeAll(toBeRemoved);
        return products;
    }

    @Override
    public int getScreenWidthDp() {
        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        return (int) (displayMetrics.widthPixels / displayMetrics.density);
    }

    private List<Product> getAllProducts() {
        products = new ArrayList<>();
        Product product1 = new Product();
        product1.setCategory("Fruits");
        product1.setName("Apple");
        product1.setPrice(50);
        product1.setImageId(R.mipmap.apple);
        Product product2 = new Product();
        product2.setCategory("Vegetables");
        product2.setName("Onion");
        product2.setPrice(500);
        product2.setImageId(R.mipmap.onion);
        Product product3 = new Product();
        product3.setCategory("Grocery");
        product3.setName("Ashirvad Atta");
        product3.setPrice(100);
        product3.setImageId(R.mipmap.ashirvad_atta);
        Product product4 = new Product();
        product4.setCategory("Misc");
        product4.setName("Cadbury");
        product4.setImageId(R.mipmap.dairy_milk);
        product4.setPrice(100);
        products.add(product1);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        return products;
    }
}

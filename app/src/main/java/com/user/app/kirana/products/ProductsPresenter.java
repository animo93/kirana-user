package com.user.app.kirana.products;

import java.util.List;

public interface ProductsPresenter {
    List<String> getProductCategories();
    void replaceCategoryFragment(int position);
    List<Product> getProductByCategory(String category);
    int getScreenWidthDp();
}

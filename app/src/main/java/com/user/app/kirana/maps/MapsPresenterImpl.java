package com.user.app.kirana.maps;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.user.app.kirana.details.DetailActivity;

import java.util.List;


public class MapsPresenterImpl implements MapsPresenter, GoogleMap.OnMarkerClickListener {

    private static final String LOG_TAG = MapsPresenterImpl.class.getSimpleName();
    private static final int REQUEST_CHECK_SETTINGS = 2;

    private MapsActivity mActivity;
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;

    //The entry points to the places API
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;

    //The entry point to the fused location provider
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;

    private boolean isLocationServicesOn = false;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private LatLng myLocation;
    private Marker mMyMarker;
    private Marker mKiranaMarker;


    MapsPresenterImpl(MapsActivity activity){
        this.mActivity = activity;
    }

    @Override
    public void init() {

        //initialize googleApiClient
        buildGoogleApiClient();

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(mActivity);

        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(mActivity);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mActivity);
    }


    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    @Override
    public void updateLocationUI() {
        Log.i(LOG_TAG,"inside updateLocationUI");
        if (mMap == null) {
            return;
        }
        try {
            if (isLocationServicesOn) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                //displayLocationSettingsRequest();
            }
            mMap.setOnMarkerClickListener(this);
        } catch (SecurityException e)  {
            Log.e(LOG_TAG, e.getMessage());
        }
    }


    @Override
    public void displayLocationSettingsRequest() {


        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        SettingsClient client = LocationServices.getSettingsClient(mActivity);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if(locationSettingsResponse != null){
                    LocationSettingsStates locationResponse = locationSettingsResponse.getLocationSettingsStates();
                    if(locationResponse.isNetworkLocationPresent() &&
                            locationResponse.isNetworkLocationUsable()){
                        Log.d(LOG_TAG, "All location settings are satisfied.");
                        // Get the current location of the device and set the position of the map.
                        isLocationServicesOn=true;
                        getDeviceLocation();

                    }
                }
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if(e instanceof ResolvableApiException) {
                    //Location settings are not satisfied , show the user a dialog
                    try{
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(mActivity,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e1) {
                        Log.e(LOG_TAG,"Unable to start location services");
                    }
                }
            }
        });
    }

    public CameraPosition getmCameraPosition() {
        return mCameraPosition;
    }

    public void setmCameraPosition(CameraPosition mCameraPosition) {
        this.mCameraPosition = mCameraPosition;
    }

    public Location getmLastKnownLocation() {
        return mLastKnownLocation;
    }

    public void setmLastKnownLocation(Location mLastKnownLocation) {
        this.mLastKnownLocation = mLastKnownLocation;
    }

    public GoogleMap getmMap() {
        return mMap;
    }

    public void setmMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    @Override
    public void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        Log.i(LOG_TAG,"inside getDeviceLocation");

        try {
            if (isLocationServicesOn) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(mActivity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            if(mLastKnownLocation==null){
                                Log.i(LOG_TAG,"got null location , going for fetching location updates");
                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest,new LocationCallback(){
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if(locationResult == null){
                                            return;
                                        }else{
                                            mLastKnownLocation = locationResult.getLastLocation();
                                            Log.i(LOG_TAG,"got last location , updating UI");
                                            setMyLocation(mLastKnownLocation);

                                            if(mMyMarker == null){
                                                MarkerOptions markerOptions = new MarkerOptions().
                                                        position(myLocation)
                                                        .title("My Location");
                                                mMyMarker = mMap.addMarker(markerOptions);
                                            }else{
                                                mMyMarker.setPosition(myLocation);
                                            }
                                            /*mMarker= mMap.addMarker(new MarkerOptions()
                                            .position(myLocation)
                                            .title("My Location"));*/
                                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                                    myLocation, DEFAULT_ZOOM));
                                            if(mKiranaMarker == null){
                                                mKiranaMarker = mMap.addMarker(new MarkerOptions()
                                                .position(getMyKirana(myLocation))
                                                .title("My Kirana"));
                                            }else{
                                                mKiranaMarker.setPosition(getMyKirana(myLocation));
                                            }

                                            /*mMap.addMarker(new MarkerOptions()
                                            .position(getMyKirana(myLocation))
                                            .title("My Kirana"));*/
                                            updateLocationUI();
                                        }
                                    }
                                },null);
                            }else{
                                Log.i(LOG_TAG,"Last location not null , updating UI");
                                setMyLocation(mLastKnownLocation);
                                /*mMap.addMarker(new MarkerOptions()
                                        .position(myLocation)
                                        .title("My Location"));*/
                                if(mMyMarker == null){
                                    MarkerOptions markerOptions = new MarkerOptions().
                                            position(myLocation)
                                            .title("My Location");
                                    mMyMarker = mMap.addMarker(markerOptions);
                                }else{
                                    mMyMarker.setPosition(myLocation);
                                }
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        myLocation, DEFAULT_ZOOM));
                                /*mMap.addMarker(new MarkerOptions()
                                        .position(getMyKirana(myLocation))
                                        .title("My Kirana"));*/
                                if(mKiranaMarker == null){
                                    mKiranaMarker = mMap.addMarker(new MarkerOptions()
                                            .position(getMyKirana(myLocation))
                                            .title("My Kirana"));
                                }else{
                                    mKiranaMarker.setPosition(getMyKirana(myLocation));
                                }
                                updateLocationUI();
                            }

                        } else {
                            Log.d(LOG_TAG, "Current location is null. Using defaults.");
                            Log.e(LOG_TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }

    }

    @Override
    public void checkLocationSettings(int requestCode, int resultCode) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(LOG_TAG, "User agreed to make required location settings changes.");
                        // Get the current location of the device and set the position of the map.
                        isLocationServicesOn=true;
                        getDeviceLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(LOG_TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    public void buildGoogleApiClient() {
        Log.i(LOG_TAG,"inside buildGoogleApiClient");
        mGoogleApiClient= new GoogleApiClient.Builder(mActivity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(mActivity)
                .addOnConnectionFailedListener(mActivity)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Call backend to fetch the nearest Kirana
     * @param myLocation
     * @return
     */
    @Override
    public LatLng getMyKirana(LatLng myLocation) {
        Log.d(LOG_TAG,"My loc lat "+myLocation.latitude+" and long "+myLocation.longitude);
        //double lat = myLocation.latitude + (180/Math.PI)*(1000/6378137);
        //double lon = myLocation.longitude + (180/Math.PI)*(1000/6378137)/Math.cos(Math.PI/180.0*myLocation.latitude);
        double lat = 18.5979738;
        double lon = 73.811631;
        Log.d(LOG_TAG,"Kirana loc lat "+lat+" and long "+lon);
        return new LatLng(lat,lon);
    }

    @Override
    public List<LatLng> getAllNearbyKiranas(LatLng myLocation, int radius) {
        return null;
    }

    @Override
    public void destroy() {
        Log.i(LOG_TAG,"inside onDestroy");
        mGoogleApiClient.disconnect();
    }

    public void setMyLocation(Location myLocation) {
        if(myLocation!=null){
            this.myLocation = new LatLng(mLastKnownLocation.getLatitude(),
                    mLastKnownLocation.getLongitude());
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        String title = marker.getTitle();
        if(title.equals("My Kirana")){
            Intent intent = new Intent(mActivity, DetailActivity.class);
            mActivity.startActivity(intent);
        }
        return false;
    }
}
